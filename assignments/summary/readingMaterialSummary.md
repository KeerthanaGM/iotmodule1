**INTERNET OF THINGS**

Internet of Things (IoT) is the interconnection through the internet of computing devices embedded in everyday objects, enabling them to send and receive data. It is the extension of connectivity of internet into physical devices. The devices which have been embedded with electronics, internet connectivity and other forms of hardware like sensors can communicate and interact with other devices over the internet. And these can be monitored remotely and controlled.

![](extras/i1.jpg)

**INDUSTRIAL INTERNET OF THINGS (IIOT)**

Industrial IoT that initially mainly referred to an industrial framework whereby a large number of devices or machines are connected and synchronized through the use of software tools and third platform technologies in a machine-to-machine and Internet of Things context, later an Industry 4.0 or Industrial Internet context.
Today IIoT is mainly used in the scope of Internet of Things applications outside of the consumer space and enterprise IoT market, as an umbrella term for applications and use cases across several industrial sectors.
The Industrial Internet of Things or IIoT is defined as “**machines, computers and people enabling intelligent industrial operations using advanced data analytics for transformational business outcomes**”.

![](extras/i2.png)

**BRIEF HISTORY OF INDUSTRY**

![](extras/i3.png)

- 1st industrial revolution – Steam and water power are used to mechanize production.
- 2nd industrial revolution – Electricity allows for mass production with assembly lines.

**3rd industrial revolution (industry 3.0)** - IT and computer technology are used to automate processes.

- In Industry 3.0, the data is stored in databases and represented in Excel.

**ARCHITECTURE**

![](extras/i4.png)

The architecture of Industry 3.0 consists of the following -
- Sensors and actuators
- Controller
- SCADA and ERP

**COMMUNICATION PROTOCOLS**

The below picture shows the communication protocols in Industry3.0

![](extras/i5.png)

**4th industrial revolution (Industry 4.0)** – Enhancing automation and connectivity with CPS.

In terms of maintenance, Industry 4.0 means lots of data can be collected through sensors and used to make decisions about repairs and upkeep. Predictive maintenance systems are even beginning to implement machine learning to determine when asset failure may be imminent and prescribe preventive measures.

**ARCHITECTURE OF INDUSTRY 4.0**

![](extras/i6.png)

- Communication Protocols - MQTT, HTTP, CoAP RFC7252, Websockets, AMQP, REST *ful* API

**CHALLENGES IN INDUSTRY 4.0**

- Cost: Industry 4.0 devices are expensive.
- Downtime: Changing hardware calls for big factory downtime.
- Unreliable devices: Might pose circumstances to invest in devices which are unproven or unreliable.

**SOLUTION** - The data from industry 3.0 (devices, meters, sensors) are collected without changes to the original device and it is send to the cloud  Industry 4.0 devices.

![](extras/i7.png)

**PROCESS OF MAKING AN INDUSTRIAL IoT PRODUCT**

- Identify most popular Industry 3.0 devices.
- Study protocols that these devices communicate.
- Get data from the Industry 3.0 device.
- Send the data to cloud for Industry 4.0 device.

**IoT TSDB TOOLS** - Stores data in time series databases.

**IoT DASHBOARDS** - Used to view data.

**IoT PLATFORMS** - Used to analyse the data. (AWS IoT, Google IoT, Azure IoT, ThingsBoard)

- Zaiper and Twilio are the two platforms used to send alerts based on the data.








